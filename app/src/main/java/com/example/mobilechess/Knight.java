package com.example.mobilechess;

public class Knight {

    /**
     * Calculates possible moves for the Knight at the specified position.
     *
     * @param justChecking Indicates whether to only check the move without making any changes.
     * @param i            The current row index of the Bishop.
     * @param j            The current column index of the Bishop.
     * @return {@code true} if there are possible moves for the Bishop, {@code false} otherwise.
     */
    public static boolean setMoves(boolean justChecking, int i, int j) {
        boolean wasThereMove = false;
        String smallerGrid[][] = Backend.convertToFigures(Backend.grid);
        int[][] moves = {
            {-2, -1}, {-2, 1},
            {-1, -2}, {-1, 2},
            {1, -2}, {1, 2},
            {2, -1}, {2, 1}
        };

        for (int[] move : moves) {
            int newRow = i + move[0];
            int newCol = j + move[1];

            if (newRow >= 0 && newRow < 8 && newCol >= 0 && newCol < 8) {
                if (Backend.grid[newRow][newCol][1].equals("") || Backend.grid[newRow][newCol][1].endsWith(Backend.currentPlayer == Backend.Players.WHITE ? "B" : "W")) {
                    if (Backend.testForPossibleMove(justChecking, i, j, newRow, newCol, Backend.currentPlayer, smallerGrid, "Kn"))
                    {
                        wasThereMove = true;
                    }
                }
            }
        }
        return wasThereMove;
    }
}
