package com.example.mobilechess;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * MainActivity class for the Mobile Chess application.
 * Handles the initialization, UI interactions, and game logic for the chess game.
 */
public class MainActivity extends AppCompatActivity {

    static String[][][] gridCopy = new String[8][8][3];
    private static final int GRID_SIZE = 8;

    public static AlertDialog dialog;

    private int gridSize;
    private ImageView lastClickedSquare;
    private int lastSquareOriginalColor;

    public static final int DARK = Color.parseColor("#A0522D");
    public static final int LIGHT = Color.parseColor("#FFFFE0");

    public static String end = "";



    /**
     * Called when the activity is first created.
     * Initializes the game board, sets up UI components, and loads saved game data.
     *
     * @param savedInstanceState If the activity is being re-initialized after previously being shut down, this Bundle contains the data it most recently supplied.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Backend.setCurrentPlayer(Backend.Players.WHITE);

        // Make activity full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Set screen orientation to portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_main);

        // Hide the ActionBar or Toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        GridLayout gridLayout = findViewById(R.id.gridLayout);
        gridLayout.setColumnCount(GRID_SIZE);
        gridLayout.setRowCount(GRID_SIZE);

        // Get screen dimensions
        WindowManager wm = getWindowManager();
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
        int screenHeight = size.y;

        // Set onClick listener to the restart button
        restartButtonSet();

        // Determine grid size based on screen dimensions
        gridSize = Math.min(screenWidth, screenHeight) / 8;
        for (int row = 0; row < GRID_SIZE; row++) {
            for (int col = 0; col < GRID_SIZE; col++) {
                ImageView square = new ImageView(this);
                GridLayout.LayoutParams params = new GridLayout.LayoutParams();
                params.width = gridSize;
                params.height = gridSize;
                params.setMargins(0, 0, 0, 0); // Add margin to separate squares
                square.setLayoutParams(params);
                square.setScaleType(ImageView.ScaleType.FIT_XY);
                square.setPadding(0, 0, 0, 0);
                square.setBackgroundColor((row + col) % 2 == 0 ? LIGHT : DARK);
                final int finalRow = row;
                final int finalCol = col;
                square.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gridCopy = Backend.deepCopy(Backend.position(finalRow, finalCol));
                        // Inside onClick method where promotion condition is met
                        if (!Backend.prom.equals("")) {
                            promotion();
                        }
                        try {
                            repaint(gridCopy);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
                gridLayout.addView(square);
            }
        }

        try {
            save();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Backend.setGrid(gridCopy);
        try {
            repaint(gridCopy);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Handles the promotion of a pawn to another piece.
     */
    private void promotion()
    {
        String[] parse = Backend.prom.split(",");
        int firstNumber = 0;
        int secondNumber = 0;
        if (parse.length == 2) {
            firstNumber = Integer.parseInt(parse[0]);
            secondNumber = Integer.parseInt(parse[1]);
        }
        // Create a dialog to show promotion options
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        View promotionDialogView = getLayoutInflater().inflate(R.layout.promotion_dialog, null);
        builder.setView(promotionDialogView);

        // Get reference to promotion options
        ImageButton promotionOption1 = promotionDialogView.findViewById(R.id.promotionOption1);
        ImageButton promotionOption2 = promotionDialogView.findViewById(R.id.promotionOption2);
        ImageButton promotionOption3 = promotionDialogView.findViewById(R.id.promotionOption3);
        ImageButton promotionOption4 = promotionDialogView.findViewById(R.id.promotionOption4);

        // Create the dialog
        dialog = builder.create();

        dialog.setCancelable(false);

        // Set click listeners for promotion options
        int finalFirstNumber = firstNumber;
        int finalSecondNumber = secondNumber;
        promotionOption1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onClickPromotion(finalFirstNumber, finalSecondNumber, "Q");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        promotionOption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onClickPromotion(finalFirstNumber, finalSecondNumber, "R");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        promotionOption3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onClickPromotion(finalFirstNumber, finalSecondNumber, "B");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        promotionOption4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onClickPromotion(finalFirstNumber, finalSecondNumber, "Kn");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        // Show the dialog
        dialog.show();
    }

    /**
     * Handles the promotion action when a promotion option is clicked.
     *
     * @param finalFirstNumber The row of the pawn to be promoted.
     * @param finalSecondNumber The column of the pawn to be promoted.
     * @param type The type of piece to which the pawn is promoted.
     */
    private void onClickPromotion(int finalFirstNumber, int finalSecondNumber, String type) throws IOException {
        String pieceType = Backend.grid[finalFirstNumber][finalSecondNumber][1];
        if (!pieceType.isEmpty()) {
            // Get the last index of the string
            int lastIndex = pieceType.length() - 1;

            // Set the new value with the last index
            Backend.grid[finalFirstNumber][finalSecondNumber][1] = type + pieceType.substring(lastIndex);
            gridCopy[finalFirstNumber][finalSecondNumber][1] = type + pieceType.substring(lastIndex);
        }
        dialog.dismiss();
        repaint(gridCopy);
    }

    /**
     * Saves the current game state and loads any previously saved game data.
     */
    private void save() throws IOException {
        String filename = "position.txt";

        if (fileExists("drawData.txt"))
        {
            loadGameData("drawData.txt");
        }


        if (fileExists("whosTurn.txt")) {
            String whosTurn = loadDataFromInternalStorage("whosTurn.txt");
            whosTurn = whosTurn.trim();
            if (whosTurn.equals("WHITE")) {
                Backend.setCurrentPlayer(Backend.Players.WHITE);
            }
            if (whosTurn.equals("BLACK")) {

                Backend.setCurrentPlayer(Backend.Players.BLACK);
            }
        }

        if (fileExists(filename)) {
            String loadedData = loadDataFromInternalStorage(filename);

            if (!loadedData.isEmpty())
            {

                gridCopy = convertStringToGrid(loadedData);
            }
            else {
                gridCopy = Backend.deepCopy(Backend.restartGame());
            }

        }
        else
        {
            String data = "";
            saveDataToInternalStorage(filename, data);
            gridCopy = Backend.deepCopy(Backend.restartGame());
        }
    }

    /**
     * Sets up the restart button and its onClick listener.
     */
    private void restartButtonSet(){
        Button restartButton = findViewById(R.id.restartButton);
        restartButton.setBackgroundColor(DARK);
        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Restart Game")
                        .setMessage("Are you sure you want to restart the game?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                gridCopy = Backend.deepCopy(Backend.restartGame());
                                try {
                                    repaint(gridCopy);
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
    }

    /**
     * Checks if a file exists in internal storage.
     *
     * @param filename The name of the file to check.
     * @return True if the file exists, false otherwise.
     */
    private boolean fileExists(String filename) {
        File file = new File(getFilesDir(), filename);
        return file.exists();
    }

    /**
     * Converts a 3D array representation of the grid to a string.
     * The output string is formatted with each row separated by a newline character,
     * cells in a row separated by an equals sign, and elements in a cell separated by commas.
     *
     * The method assigns "L" or "D" to the first element of each cell based on the position's color.
     * The second element of each cell is appended directly to the output string.
     *
     * @param grid The 3D array representation of the grid.
     * @return The string representation of the grid.
     */
    private String convertGridToString(String[][][] grid) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                for (int k = 0; k < 3; k++)
                {
                    if (k == 1)
                    {
                        sb.append(grid[i][j][k] + ",");
                    }
                    if (k == 0)
                    {
                        if((i + j) % 2 == 0)
                        {
                            sb.append("L,");
                        }
                        else {
                            sb.append("D,");
                        }
                    }
                }
                sb.append("=");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Converts a string representation of the grid to a 3D array.
     * Each cell in the grid is initialized to an empty string before parsing the input data.
     *
     * @param data The string representation of the grid. Each row is separated by a newline character,
     *             cells in a row are separated by an equals sign, and elements in a cell are separated by commas.
     * @return The 3D array representation of the grid.
     */
    private String[][][] convertStringToGrid(String data) {
        String[] rows = data.split("\n");
        String[][][] grid = new String[8][8][3];

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    grid[i][j][k] = "";
                }
            }
        }

        for (int i = 0; i < 8; i++) {
            String[] cells = rows[i].split("=");
            for (int j = 0; j < cells.length; j++) {
                String q[] = cells[j].split(",");
                for (int k = 0; k < q.length; k++)
                {
                    grid[i][j][k] = q[k];
                }
            }
        }
        return grid;
    }


    /**
     * Repaints the game board based on the current state of the grid.
     *
     * @param grid The current state of the grid.
     */
    private void repaint(String[][][] grid) throws IOException {
        GridLayout gridLayout = findViewById(R.id.gridLayout);


        String filename = "position.txt";
        String data = convertGridToString(grid);
        if (!(dialog != null && dialog.isShowing())) {

            saveDataToInternalStorage(filename, data);
            saveGameData("drawData.txt");
            saveDataToInternalStorage("whosTurn.txt", String.valueOf(Backend.currentPlayer));
        }

        // Clear existing images from all squares
        clear(gridLayout);

        // Repaint squares based on the grid content
        rapaintGrid(grid, gridLayout);

        if (!end.equals(""))
        {
            Backend.blockAllMoves = true;
            showPanelForEnd(end);
            end = "";
        }
    }

    /**
     * Repaints the grid based on the provided 3D array and GridLayout.
     * This method iterates through the grid, sets the background color,
     * assigns the appropriate image resource, and optionally draws a circle
     * on the square if specified in the grid data.
     *
     * @param grid The 3D array representation of the grid.
     * @param gridLayout The GridLayout containing the squares of the chessboard.
     */
    private void rapaintGrid(String[][][] grid, GridLayout gridLayout)
    {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                String[] parsed = grid[i][j];
                ImageView squareOnPosition = (ImageView) gridLayout.getChildAt(i * GRID_SIZE + j);

                // Set background color
                if (parsed[0].equals("L")) {
                    squareOnPosition.setBackgroundColor(LIGHT);
                } else if (parsed[0].equals("D")) {
                    squareOnPosition.setBackgroundColor(DARK);
                } else if (parsed[0].equals("G")) {
                    squareOnPosition.setBackgroundColor(Color.GRAY);
                }

                // Set image resource
                if (parsed.length >= 2) {
                    images(parsed, squareOnPosition);
                }

                // Create a new bitmap after layout is complete
                if (parsed.length >= 3 && parsed[2].equals("C")) {
                    circles(squareOnPosition);
                }
            }
        }
    }

    /**
     * Sets the image resource of the given ImageView based on the piece type.
     * The piece type is determined by the second element in the parsed array.
     *
     * @param parsed The array containing the piece type information.
     * @param squareOnPosition The ImageView representing a square on the chessboard.
     */
    private void images(String[] parsed, ImageView squareOnPosition)
    {
        switch (parsed[1]) {
            case "PW":
                squareOnPosition.setImageResource(R.drawable.pawn_white);
                break;
            case "PB":
                squareOnPosition.setImageResource(R.drawable.pawn_black);
                break;
            case "BW":
                squareOnPosition.setImageResource(R.drawable.bishop_white);
                break;
            case "BB":
                squareOnPosition.setImageResource(R.drawable.bishop_black);
                break;
            case "KnW":
                squareOnPosition.setImageResource(R.drawable.knight_white);
                break;
            case "KnB":
                squareOnPosition.setImageResource(R.drawable.knight_black);
                break;
            case "KiW":
                squareOnPosition.setImageResource(R.drawable.king_white);
                break;
            case "KiB":
                squareOnPosition.setImageResource(R.drawable.king_black);
                break;
            case "QW":
                squareOnPosition.setImageResource(R.drawable.queen_white);
                break;
            case "QB":
                squareOnPosition.setImageResource(R.drawable.queen_black);
                break;
            case "RW":
                squareOnPosition.setImageResource(R.drawable.rook_white);
                break;
            case "RB":
                squareOnPosition.setImageResource(R.drawable.rook_black);
                break;
        }
    }

    /**
     * Draws a circle on the given ImageView.
     * This method creates a bitmap, draws the current image of the square onto the canvas,
     * and then draws an unfilled black circle outline on top of it.
     *
     * @param squareOnPosition The ImageView representing a square on the chessboard.
     */
    private void circles(ImageView squareOnPosition)
    {
        squareOnPosition.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // Ensure we remove the listener to avoid multiple calls if layout changes again
                squareOnPosition.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width = squareOnPosition.getWidth();
                int height = squareOnPosition.getHeight();
                if (width > 0 && height > 0) {
                    // Create bitmap and draw circle here
                    Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);

                    // Draw the pawn image onto the canvas
                    squareOnPosition.draw(canvas);

                    // Draw unfilled black circle outline on the square
                    Paint paint = new Paint();
                    paint.setColor(Color.BLACK);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setStrokeWidth(10);
                    canvas.drawCircle(width / 2f, height / 2f, Math.min(width, height) / 3f, paint);

                    // Set the bitmap with the drawn pawn and circle as the image resource of the square's ImageView
                    squareOnPosition.setImageBitmap(bitmap);
                }
            }
        });
    }

    /**
     * Clears all images from the squares in the given GridLayout.
     * This method sets the image resource of each square's ImageView to 0,
     * effectively removing any images displayed on the squares.
     *
     * @param gridLayout The GridLayout containing the squares of the chessboard.
     */
    private void clear(GridLayout gridLayout)
    {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                ImageView squareOnPosition = (ImageView) gridLayout.getChildAt(i * GRID_SIZE + j);
                squareOnPosition.setImageResource(0);
            }
        }
    }

    /**
     * Saves data to internal storage.
     *
     * @param filename The name of the file to save the data.
     * @param data     The data to be saved.
     */
    private void saveDataToInternalStorage(String filename, String data) throws IOException {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(filename, MODE_PRIVATE);
            fos.write(data.getBytes());
            fos.close();
        } catch (IOException ignored) {
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
    }

    /**
     * Loads data from internal storage.
     *
     * @param filename The name of the file to load the data from.
     * @return The loaded data as a string.
     */
    private String loadDataFromInternalStorage(String filename) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            FileInputStream fis = openFileInput(filename);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            bufferedReader.close();
            isr.close();
            fis.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return stringBuilder.toString();
    }

    public void saveGameData(String filename) throws IOException {
        String toSave = arrayToString(Backend.fiftyMoveRule, Backend.moveCounter, Backend.gridHistory);
        saveDataToInternalStorage(filename, toSave);
    }

    /**
     * Loads game data from internal storage.
     *
     * @param filename The name of the file to load the data from.
     */
    public void loadGameData(String filename) {
        String loaded = loadDataFromInternalStorage(filename);
        if (loaded != null && !loaded.isEmpty()) {
            Object[] result = stringToArray(loaded);
            Backend.fiftyMoveRule = (int) result[0];
            Backend.moveCounter = (int) result[1];
            Backend.gridHistory = (String[][][]) result[2];
            // Now you have x, y, and the array loaded from the file
            System.out.println("Loaded fiftyMoveRule: " + Backend.fiftyMoveRule);
            System.out.println("Loaded moveCounter: " + Backend.moveCounter);
            System.out.println("Loaded gridHistory: " + Backend.gridHistory);
        } else {
            System.out.println("Failed to load game data.");
        }
    }

    /**
     * Converts a 3-dimensional string array into a formatted string representation.
     * This method concatenates the dimensions and elements of the array into a single string,
     * separating elements by commas.
     *
     * @param x The first dimension value.
     * @param y The second dimension value.
     * @param array The 3-dimensional string array to convert.
     * @return A formatted string representation of the array.
     */
    public static String arrayToString(int x, int y, String[][][] array) {
        StringBuilder sb = new StringBuilder();
        sb.append(x).append(",").append(y).append(",");
        for (String[][] row : array) {
            for (String[] subRow : row) {
                for (String item : subRow) {
                    sb.append(item != null ? item : "").append(",");
                }
            }
        }
        return sb.toString();
    }

    /**
     * Converts a formatted string representation back into a 3-dimensional string array.
     * This method splits the input string by commas and reconstructs the original array dimensions
     * and elements based on the parsed values.
     *
     * @param longString The formatted string representation of the array.
     * @return An object array containing the first and second dimension values, and the reconstructed 3-dimensional string array.
     */
    public static Object[] stringToArray(String longString) {
        String[] parts = longString.split(",");
        int x = Integer.parseInt(parts[0]);
        int y = Integer.parseInt(parts[1]);
        String[][][] array = new String[70][8][8];
        int index = 2;
        for (int i = 0; i < 70; i++) {
            for (int j = 0; j < 8; j++) {
                for (int k = 0; k < 8; k++) {
                    if (index < parts.length) {
                        array[i][j][k] = parts[index++];
                    } else {
                        array[i][j][k] = null;
                    }
                }
            }
        }
        return new Object[]{x, y, array};
    }

    /**
     * Displays a short-duration toast message on the screen.
     *
     * @param message The message to display in the toast.
     */
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Shows an alert dialog panel with the specified text message.
     *
     * @param text The text message to display in the panel.
     */
    public void showPanelForEnd(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View panelView = getLayoutInflater().inflate(R.layout.panel_layout, null);

        TextView textViewMessage = panelView.findViewById(R.id.textViewMessage);
        textViewMessage.setText(text);

        builder.setView(panelView);
        dialog = builder.create();
        dialog.show();
    }

}