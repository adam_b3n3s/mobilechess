package com.example.mobilechess;

import java.util.Scanner;

public class Main {
    static String[][][] gridCopy = new String[8][8][3];
    public static void main(String[] args) {
        
        Backend.setCurrentPlayer(Backend.Players.WHITE);
        
        System.out.println();
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Do you want long or short output format? (long/short): ");
        
        String userInput = scanner.nextLine();
        
        Boolean LONG = false;
        
        while (!(userInput.equalsIgnoreCase("long") || userInput.equalsIgnoreCase("short")))
        {
            userInput = scanner.nextLine();
        }
        if (userInput.equalsIgnoreCase("long"))
        {
            LONG = true;
        }
        if (userInput.equalsIgnoreCase("short"))
        {
            LONG = false;
        }
        
        
        System.out.println();
        System.out.println("Enter move in format \"number, number\".");
        System.out.println("If you want to end the program enter \"End\".");
        System.out.println("If you want to restart the game enter \"Restart\".");
        System.out.println();
        gridCopy = Backend.deepCopy(Backend.restartGame());
        if(LONG)
        {
            printAll();
        }
        else{
            print();
        }
        System.out.print("Enter move: ");

        userInput = scanner.nextLine();
        String[] coordinates = userInput.split(",");
        if (coordinates.length == 2) {
            try {
                int x = Integer.parseInt(coordinates[0].trim());
                int y = Integer.parseInt(coordinates[1].trim());
                gridCopy = Backend.deepCopy(Backend.position(x, y));
                
                if (!Backend.prom.equals("")) {
                    String[] parse = Backend.prom.split(",");
                    int firstNumber = 0;
                    int secondNumber = 0;
                    if (parse.length == 2) {
                        try {
                            firstNumber = Integer.parseInt(parse[0]);
                            secondNumber = Integer.parseInt(parse[1]);
                        } catch (NumberFormatException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    int finalFirstNumber = firstNumber;
                    int finalSecondNumber = secondNumber;
                    System.out.print("Enter promotion (Q/R/B/K): ");
                    userInput = scanner.nextLine();
                    while (!(userInput.equalsIgnoreCase("Q") || userInput.equalsIgnoreCase("R") || userInput.equalsIgnoreCase("B") || userInput.equalsIgnoreCase("K")))
                    {
                        userInput = scanner.nextLine();
                    }
                    if (userInput.equalsIgnoreCase("Q"))
                    {
                        Backend.grid[finalFirstNumber][finalSecondNumber][1] = "Q" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                        gridCopy[finalFirstNumber][finalSecondNumber][1] = "Q" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                    }
                    if (userInput.equalsIgnoreCase("R"))
                    {
                        Backend.grid[finalFirstNumber][finalSecondNumber][1] = "R" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                        gridCopy[finalFirstNumber][finalSecondNumber][1] = "R" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                    }
                    if (userInput.equalsIgnoreCase("B"))
                    {
                        Backend.grid[finalFirstNumber][finalSecondNumber][1] = "B" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                        gridCopy[finalFirstNumber][finalSecondNumber][1] = "B" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                    }
                    if (userInput.equalsIgnoreCase("K"))
                    {
                        Backend.grid[finalFirstNumber][finalSecondNumber][1] = "K" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                        gridCopy[finalFirstNumber][finalSecondNumber][1] = "K" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                    }
                }
                
                if(LONG)
                {
                    printAll();
                }
                else{
                    print();
                }
                
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        while (!userInput.equalsIgnoreCase("end"))
        {
            if (userInput.equalsIgnoreCase("restart"))
            {
                gridCopy = Backend.deepCopy(Backend.restartGame());
                if(LONG)
                {
                    printAll();
                }
                else{
                    print();
                }
            }
            System.out.print("Enter move: ");
            userInput = scanner.nextLine();
            coordinates = userInput.split(",");
            if (coordinates.length == 2) {
                try {
                    int x = Integer.parseInt(coordinates[0].trim());
                    int y = Integer.parseInt(coordinates[1].trim());
                    gridCopy = Backend.deepCopy(Backend.position(x, y));
                    
                    if (!Backend.prom.equals("")) {
                        String[] parse = Backend.prom.split(",");
                        int firstNumber = 0;
                        int secondNumber = 0;
                        if (parse.length == 2) {
                            try {
                                firstNumber = Integer.parseInt(parse[0]);
                                secondNumber = Integer.parseInt(parse[1]);
                            } catch (NumberFormatException e) {
                                throw new RuntimeException(e);
                            }
                        }
                        int finalFirstNumber = firstNumber;
                        int finalSecondNumber = secondNumber;
                        System.out.print("Enter promotion (Q/R/B/K): ");
                        userInput = scanner.nextLine();
                        while (!(userInput.equalsIgnoreCase("Q") || userInput.equalsIgnoreCase("R") || userInput.equalsIgnoreCase("B") || userInput.equalsIgnoreCase("K")))
                        {
                            userInput = scanner.nextLine();
                        }
                        if (userInput.equalsIgnoreCase("Q"))
                        {
                            Backend.grid[finalFirstNumber][finalSecondNumber][1] = "Q" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                            gridCopy[finalFirstNumber][finalSecondNumber][1] = "Q" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                        }
                        if (userInput.equalsIgnoreCase("R"))
                        {
                            Backend.grid[finalFirstNumber][finalSecondNumber][1] = "R" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                            gridCopy[finalFirstNumber][finalSecondNumber][1] = "R" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                        }
                        if (userInput.equalsIgnoreCase("B"))
                        {
                            Backend.grid[finalFirstNumber][finalSecondNumber][1] = "B" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                            gridCopy[finalFirstNumber][finalSecondNumber][1] = "B" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                        }
                        if (userInput.equalsIgnoreCase("K"))
                        {
                            Backend.grid[finalFirstNumber][finalSecondNumber][1] = "K" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                            gridCopy[finalFirstNumber][finalSecondNumber][1] = "K" + Backend.grid[finalFirstNumber][finalSecondNumber][1].substring(1);
                        }
                    }
                    
                    
                    if(LONG)
                    {
                        printAll();
                    }
                    else{
                        print();
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                
                
            }
            
        }
        scanner.close();
    }
    public static void printAll(){
        int len = 0;
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                if (gridCopy[i][j][0].length() + gridCopy[i][j][1].length() + gridCopy[i][j][2].length() > len){
                    len = gridCopy[i][j][0].length() + gridCopy[i][j][1].length() + gridCopy[i][j][2].length();
                }
            }
        }
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                while (gridCopy[i][j][0].length() + gridCopy[i][j][1].length() + gridCopy[i][j][2].length() < len)
                {
                    gridCopy[i][j][2] += " ";
                }
            }
        }
        String toLen = "-";
        for(int j = 0; j < 8; j++)
        {
            toLen += "-------";
            int x = 0;
            while(x < len)
            {
                x++;
                toLen += "-";
            }
        }
        System.out.println(toLen + " ");
        for(int i = 0; i < 8; i++)
        {
            System.out.print("| ");
            for(int j = 0; j < 8; j++)
            {
                System.out.print(i + "," + j + ":");
                System.out.print(gridCopy[i][j][0] + gridCopy[i][j][1] + gridCopy[i][j][2] + " | ");
                
            }
            System.out.println("");
            System.out.println(toLen + " ");
        }
    }
    
    public static void print(){
        System.out.println(" -------------------------------------------------");
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                String y = "   ";
                y = gridCopy[i][j][1];
                while (y.length() < 3)
                {
                    y += " ";
                }
                if (j != 0)
                {
                    System.out.print(y + " | ");
                }
                else
                {
                    System.out.print(" | " + y + " | ");
                }
                
            }
            System.out.println();
            System.out.println(" -------------------------------------------------");
        }
    }
    public static String setPromotion(){
        return "QW";
    }
}