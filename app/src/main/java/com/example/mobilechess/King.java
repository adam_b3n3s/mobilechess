package com.example.mobilechess;

public class King {
    
    public static boolean whiteKingMove = false;
    public static boolean blackKingMove = false;

    /**
     * Checks if the King of the specified color is in check.
     *
     * @param color       The color of the King ('WHITE' or 'BLACK').
     * @param smallerGrid The 2D array representing the current chess board state.
     * @return {@code true} if the King of the specified color is in check, {@code false} otherwise.
     */
    public static boolean isCheck(Backend.Players color, String smallerGrid[][])
    {
        int i = 0;
        int j = 0;
        
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if (smallerGrid[x][y].equals("Ki" + (color == Backend.Players.WHITE ? "W" : "B")))
                {
                    i = x;
                    j = y;
                    break;
                }
            }
        }
        return Backend.isAttackedByOpposite(i, j, color, smallerGrid);
    }

    /**
     * Checks if the King of the specified color is in checkmate.
     *
     * @param color       The color of the King ('WHITE' or 'BLACK').
     * @param smallerGrid The 2D array representing the current chess board state.
     * @return {@code true} if the King of the specified color is in checkmate, {@code false} otherwise.
     */
    public static boolean isCheckmate(Backend.Players color, String smallerGrid[][])
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if (smallerGrid[x][y].endsWith((color == Backend.Players.WHITE ? "W" : "B")))
                {
                    if (Backend.isThereAnyPossibleMove(x,y, color, smallerGrid))
                    {
                        return false;
                    }
                }
            }
        }

        int i = 0;
        int j = 0;
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if (smallerGrid[x][y].equals("Ki" + (color == Backend.Players.WHITE ? "W" : "B")))
                {
                    boolean thereIsMove = Backend.isThereAnyPossibleMove(x,y, color, smallerGrid);
                    i = x;
                    j = y;
                    break;
                }
            }
        }
        return Backend.isAttackedByOpposite(i, j, color, smallerGrid);
    }

    /**
     * Checks if the game is drawn due to stalemate.
     *
     * @param color       The color of the player ('WHITE' or 'BLACK').
     * @param smallerGrid The 2D array representing the current chess board state.
     * @return {@code true} if the game is drawn due to stalemate, {@code false} otherwise.
     */
    public static boolean isDrawByStalemate(Backend.Players color, String smallerGrid[][])
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if (smallerGrid[x][y].endsWith((color == Backend.Players.WHITE ? "W" : "B")))
                {
                    if (Backend.isThereAnyPossibleMove(x,y, color, smallerGrid))
                    {
                        return false;
                    }
                }
            }
        }

        int i = 0;
        int j = 0;

        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if (smallerGrid[x][y].equals("Ki" + (color == Backend.Players.WHITE ? "W" : "B")))
                {
                    boolean thereIsMove = Backend.isThereAnyPossibleMove(x,y, color, smallerGrid);
                    i = x;
                    j = y;
                    break;
                }
            }
        }

        return !Backend.isAttackedByOpposite(i, j, color, smallerGrid);
    }

    /**
     * Checks if the game is drawn due to repetition of moves.
     *
     * @return {@code true} if the game is drawn due to repetition of moves, {@code false} otherwise.
     */
    public static boolean isDrawByRepetition()
    {
        int count = 0;
        int cnt = 0;
        for (String[][] gridhis : Backend.gridHistory)
        {
            if (Backend.compareGrids(gridhis, Backend.convertToFigures(Backend.grid)))
            {
                count++;
            }
            cnt++;
        }
        return count >= 3;
    }

    /**
     * Checks if the game is drawn due to the fifty-move rule.
     *
     * @return {@code true} if the game is drawn due to the fifty-move rule, {@code false} otherwise.
     */
    public static boolean isDrawByFiftyMoveRule()
    {
        return Backend.fiftyMoveRule >= 50;
    }

    /**
     * Checks if the game is drawn due to insufficient material.
     *
     * @return {@code true} if the game is drawn due to insufficient material, {@code false} otherwise.
     */
    public static boolean isDrawByInsufficientMaterial() {
        int size = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (Backend.grid[i][j][1] != null && !(Backend.grid[i][j][1].equals(""))) {
                    size++;
                }
            }
        }
        if (size > 4) {
            return false;
        }
        if (size == 2) {
            return true;
        }
        if (size == 3) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (Backend.grid[i][j][1] != null && !(Backend.grid[i][j][1].equals("")) && (Backend.grid[i][j][1].startsWith("Kn") || Backend.grid[i][j][1].startsWith("B"))) {
                        return true;
                    }
                }
            }
        }
        if (size == 4) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (Backend.grid[i][j][1] != null && !(Backend.grid[i][j][1].equals("")) && Backend.grid[i][j][1].startsWith("B")) {
                        for (int k = 0; k < 8; k++) {
                            for (int l = 0; l < 8; l++) {
                                if (Backend.grid[k][l][1] != null && !(Backend.grid[k][l][1].equals("")) && Backend.grid[k][l][1].startsWith("B")) {
                                    if (Backend.grid[i][j][1].charAt(Backend.grid[i][j][1].length() - 1) == Backend.grid[k][l][1].charAt(Backend.grid[k][l][1].length() - 1)) {
                                        if ((i + j) % 2 == (k + l) % 2) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Calculates possible moves for the King at the specified position.
     *
     * @param justChecking Indicates whether to only check the move without making any changes.
     * @param i            The current row index of the Bishop.
     * @param j            The current column index of the Bishop.
     * @return {@code true} if there are possible moves for the Bishop, {@code false} otherwise.
     */
    public static boolean setMoves(boolean justChecking, int i, int j) {
        boolean wasThereMove = false;
        String smallerGrid[][] = Backend.convertToFigures(Backend.grid);
        int[][] offsets = {
            {-1, -1}, {-1, 0}, {-1, 1},
            {0, -1},           {0, 1},
            {1, -1}, {1, 0}, {1, 1}
        };

        for (int[] offset : offsets) {
            int newRow = i + offset[0];
            int newCol = j + offset[1];

            if (newRow >= 0 && newRow < 8 && newCol >= 0 && newCol < 8) {
                if (Backend.grid[newRow][newCol][1].equals("") || Backend.grid[newRow][newCol][1].endsWith(Backend.currentPlayer == Backend.Players.WHITE ? "B" : "W")) {
                    if (Backend.testForPossibleMove(justChecking, i, j, newRow, newCol, Backend.currentPlayer, smallerGrid, "Ki"))
                    {
                        wasThereMove = true;
                    }
                }
            }
        }
        // Castling
        castling(i, j, justChecking, wasThereMove, smallerGrid);
        return wasThereMove;
    }

    /**
     * Performs castling move if conditions are met.
     *
     * @param i            The current row index of the King.
     * @param j            The current column index of the King.
     * @param justChecking Indicates whether to only check the move without making any changes.
     * @param wasThereMove Indicates whether a move was already found.
     * @param smallerGrid  The 2D array representing the current chess board state.
     */
    private static void castling(int i, int j, boolean justChecking, boolean wasThereMove, String[][] smallerGrid)
    {
        if (Backend.currentPlayer == Backend.Players.WHITE) {
            if (i == 7 && j == 4) {
                if (!whiteKingMove) {
                    if (!Rook.SZrookMove && Backend.grid[7][0][1].equals("RW")) {
                        boolean canCastle = true;
                        for (int k = 1; k < 4; k++) {
                            if (!Backend.grid[7][k][1].equals("")) {
                                canCastle = false;
                                break;
                            }
                        }
                        if (canCastle) {
                            System.out.println("Can castle");
                            if (Backend.testForPossibleMove(justChecking, 7, 4, 7, 3, Backend.currentPlayer, smallerGrid, "Ki"))
                            {
                                if (Backend.testForPossibleMove(justChecking, 7, 4, 7, 2, Backend.currentPlayer, smallerGrid, "Ki"))
                                {
                                    wasThereMove = true;
                                }
                            }
                        }
                    }
                    if (!Rook.SSrookMove && Backend.grid[7][7][1].equals("RW")) {
                        boolean canCastle = true;
                        for (int k = 5; k < 7; k++) {
                            if (!Backend.grid[7][k][1].equals("")) {
                                canCastle = false;
                                break;
                            }
                        }
                        if (canCastle) {
                            if (Backend.testForPossibleMove(justChecking, 7, 4, 7, 5, Backend.currentPlayer, smallerGrid, "Ki"))
                            {
                                if (Backend.testForPossibleMove(justChecking, 7, 4, 7, 6, Backend.currentPlayer, smallerGrid, "Ki"))
                                {
                                    wasThereMove = true;
                                }

                            }
                        }
                    }
                }
            }
        } else {
            if (i == 0 && j == 4) {
                if (!blackKingMove) {
                    if (!Rook.ZZrookMove && Backend.grid[0][0][1].equals("RB")) {
                        boolean canCastle = true;
                        for (int k = 1; k < 4; k++) {
                            if (!Backend.grid[0][k][1].equals("")) {
                                canCastle = false;
                                break;
                            }
                        }
                        if (canCastle) {
                            System.out.println("Can castle");
                            if (Backend.testForPossibleMove(justChecking, 0, 4, 0, 3, Backend.currentPlayer, smallerGrid, "Ki"))
                            {
                                if (Backend.testForPossibleMove(justChecking, 0, 4, 0, 2, Backend.currentPlayer, smallerGrid, "Ki"))
                                {
                                    wasThereMove = true;
                                }
                            }
                        }
                    }
                    if (!Rook.ZSrookMove && Backend.grid[0][7][1].equals("RB")) {
                        boolean canCastle = true;
                        for (int k = 5; k < 7; k++) {
                            if (!Backend.grid[0][k][1].equals("")) {
                                canCastle = false;
                                break;
                            }
                        }
                        if (canCastle) {
                            System.out.println("Can castle");
                            if (Backend.testForPossibleMove(justChecking, 0, 4, 0, 5, Backend.currentPlayer, smallerGrid, "Ki"))
                            {
                                if (Backend.testForPossibleMove(justChecking, 0, 4, 0, 6, Backend.currentPlayer, smallerGrid, "Ki"))
                                {
                                    wasThereMove = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
