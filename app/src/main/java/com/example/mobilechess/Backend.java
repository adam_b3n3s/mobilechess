package com.example.mobilechess;


public class Backend {
    public static String[][][] grid = new String[8][8][3];
    
    static int selectedPlayerX = -1;
    static int selectedPlayerY = -1;
    
    public static String[][][] gridHistory = new String[70][8][8];
    
    public static int moveCounter = 0;
    
    public static int fiftyMoveRule = 0;

    public static boolean blockAllMoves = false;

    public enum Players {
        BLACK,
        WHITE
    }
    public static Players currentPlayer;

    /**
     * Prints the grid history to the console. This method iterates over a specific portion
     * of the grid history array and prints each element. Null elements are represented as 'n',
     * while non-null elements are enclosed in dots (.).
     */
    public static void printGridHistory() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 8; j++) {
                for (int k = 0; k < 8; k++) {
                    if (gridHistory[i][j][k] == null)
                    {
                        System.out.print("n ");
                    }
                    else
                    {
                        System.out.print("." + gridHistory[i][j][k] + ". ");
                    }
                }
                System.out.println();
            }
            System.out.println("________________________");
        }
    }

    /**
     * Switches the current player between BLACK and WHITE.
     * If the current player is BLACK, it switches to WHITE, and vice versa.
     */
    public static void switchPlayer() {
        // Switch the current player
        currentPlayer = (currentPlayer == Players.BLACK) ? Players.WHITE : Players.BLACK;
    }

    /**
     * Sets the current player to the specified player.
     * @param player The player to set as the current player.
     */
    public static void setCurrentPlayer(Players player) {
        // set the color of player
        currentPlayer = player;
    }
    
    public static String prom = "";

    /**
     * Adjusts the grid state based on the selected position (x, y).
     * If the position has a circle ("C"), invokes moveThere() or removeAllCircles() accordingly.
     * Sets the original color of squares and adjusts based on the piece type and current player's turn.
     * @param x The x-coordinate of the selected position.
     * @param y The y-coordinate of the selected position.
     * @return The updated grid state after adjustments.
     */
    public static String[][][] position(int x, int y) {
        prom = "";
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (i == x && j == y)
                {
                    if (grid[i][j][2].equals("C"))
                    {
                        moveThere(i, j, x, y);
                    }
                    else{
                        removeAllCircles();
                    }
                    if (grid[i][j][0].equals("G"))
                    {
                        setToOriginalColor(i, j);
                    }
                    else if (!grid[i][j][1].equals(""))
                    {
                        setWhenEmpty(i, j);
                    }
                }
                else {
                    setToOriginalColor(i, j);
                }
            }
        }
        return grid;
    }

    /**
     * Sets the original color of the square at position (i, j) and adjusts moves based on the piece type and current player's turn.
     * If the piece belongs to the current player, sets the square color to "G" and initializes valid moves for the piece type.
     * @param i The row index of the square.
     * @param j The column index of the square.
     */
    private static void setWhenEmpty(int i, int j)
    {
        String piece = grid[i][j][1];
        if (piece.endsWith("W") && currentPlayer == Players.WHITE) {
            grid[i][j][0] = "G";
            selectedPlayerX = i;
            selectedPlayerY = j;
            if (piece.equals("PW"))
            {
                Pawn.setMoves(false, i, j);
            }
            if (piece.equals("RW"))
            {
                Rook.setMoves(false, i, j);
            }
            if (piece.equals("BW"))
            {
                Bishop.setMoves(false, i, j);
            }
            if (piece.equals("QW"))
            {
                Queen.setMoves(false, i, j);
            }
            if (piece.equals("KiW"))
            {
                King.setMoves(false, i, j);
            }
            if (piece.equals("KnW"))
            {
                Knight.setMoves(false, i, j);
            }
        } else if (piece.endsWith("B") && currentPlayer == Players.BLACK) {
            grid[i][j][0] = "G";
            selectedPlayerX = i;
            selectedPlayerY = j;
            if (piece.equals("PB"))
            {
                Pawn.setMoves(false, i, j);
            }
            if (piece.equals("RB"))
            {
                Rook.setMoves(false, i, j);
            }
            if (piece.equals("BB"))
            {
                Bishop.setMoves(false, i, j);
            }
            if (piece.equals("QB"))
            {
                Queen.setMoves(false, i, j);
            }
            if (piece.equals("KiB"))
            {
                King.setMoves(false, i, j);
            }
            if (piece.equals("KnB"))
            {
                Knight.setMoves(false, i, j);
            }
        }
    }

    /**
     * Moves the piece from the selected position (selectedPlayerX, selectedPlayerY) to the target position (i, j).
     * Updates move counters, handles special moves like promotion and en passant, adjusts piece positions on the grid,
     * checks for castling moves for kings, updates rook positions accordingly, switches players, and handles game end conditions.
     *
     * @param i The row index of the target position.
     * @param j The column index of the target position.
     * @param x The row index of the selected position.
     * @param y The column index of the selected position.
     */
    private static void moveThere(int i, int j, int x, int y)
    {
        moveCounter++;
        if (moveCounter == 64)
        {
            moveCounter = 0;
        }
        fiftyMoveRule++;
        // Reset fifty move rule counter
        if ((!(grid[i][j][1].equals("")) && !(grid[i][j][1].equals("EP"))) || grid[selectedPlayerX][selectedPlayerY][1].equals("PW") || grid[selectedPlayerX][selectedPlayerY][1].equals("PB"))
        {
            fiftyMoveRule = 0;
        }
        // Start promotion
        promotion(i, j);
        // End promotion

        // Start en passant:
        enPassant(i, j, x, y);
        // End en passant.

        System.out.println("Fifty move counter: " + fiftyMoveRule);

        grid[i][j][1] = grid[selectedPlayerX][selectedPlayerY][1];
        if (grid[i][j][1].equals("KiW") && selectedPlayerX == 7 && selectedPlayerY == 4)
        {
            if (j - selectedPlayerY == 2)
            {
                grid[7][7][1] = "";
                grid[7][5][1] = "RW";
            }
            if (selectedPlayerY - j == 2)
            {
                grid[7][0][1] = "";
                grid[7][3][1] = "RW";
            }
            King.whiteKingMove = true;
        }
        if (grid[i][j][1].equals("KiB") && selectedPlayerX == 0 && selectedPlayerY == 4)
        {
            if (j - selectedPlayerY == 2)
            {
                grid[0][7][1] = "";
                grid[0][5][1] = "RB";
            }
            if (selectedPlayerY - j == 2)
            {
                grid[0][0][1] = "";
                grid[0][3][1] = "RB";
            }
            King.blackKingMove = true;
        }
        rooksMoved(i, j);
        grid[selectedPlayerX][selectedPlayerY][1] = "";
        setToOriginalColor(selectedPlayerX, selectedPlayerY);
        selectedPlayerX = -1;
        selectedPlayerY = -1;
        removeAllCircles();
        switchPlayer();
        String[][] smallerGrid = convertToFigures(grid);
        gridHistory[moveCounter] = convertToFiguresWithoutEnPassant(grid);
        handleEnd(smallerGrid);
    }

    /**
     * Checks if a rook has moved based on its initial position and updates the corresponding static flags in the Rook class.
     *
     * @param i The row index of the current position.
     * @param j The column index of the current position.
     */
    private static void rooksMoved(int i, int j)
    {
        if (selectedPlayerX == 0 && selectedPlayerY == 0 && grid[i][j][1].equals("RB"))
        {
            Rook.ZZrookMove = true;
        }
        if (selectedPlayerX == 0 && selectedPlayerY == 7 && grid[i][j][1].equals("RB"))
        {
            Rook.ZSrookMove = true;
        }
        if (selectedPlayerX == 7 && selectedPlayerY == 0 && grid[i][j][1].equals("RW"))
        {
            Rook.SZrookMove = true;
        }
        if (selectedPlayerX == 7 && selectedPlayerY == 7 && grid[i][j][1].equals("RW"))
        {
            Rook.SSrookMove = true;
        }
    }

    /**
     * Handles pawn promotion when a pawn reaches the opposite end of the board (8th rank for white, 1st rank for black).
     * Resets the fifty move rule counter upon promotion.
     *
     * @param i The row index of the current position.
     * @param j The column index of the current position.
     */
    private static void promotion(int i, int j)
    {
        if ((grid[selectedPlayerX][selectedPlayerY][1].equals("PW") ||
                grid[selectedPlayerX][selectedPlayerY][1].equals("PB"))
                && (i == 0 || i == 7)) {
            prom = Integer.toString(i) + "," + Integer.toString(j);
            fiftyMoveRule = 0;
        }
    }

    /**
     * Handles en passant move for pawns. Updates the grid if the current move qualifies for en passant.
     * Resets the fifty move rule counter upon an en passant move.
     *
     * @param i The row index of the current position.
     * @param j The column index of the current position.
     * @param x The row index of the selected position.
     * @param y The column index of the selected position.
     */
    private static void enPassant(int i, int j, int x, int y)
    {
        removeAllEnPassants();
        if ((grid[selectedPlayerX][selectedPlayerY][1].equals("PW") ||
                grid[selectedPlayerX][selectedPlayerY][1].equals("PB"))
                && Math.abs(selectedPlayerX - i) == 2) {
            grid[(selectedPlayerX + i) / 2][y][1] = "EP";
            fiftyMoveRule = 0;
        }
        if ((grid[selectedPlayerX][selectedPlayerY][1].equals("PW") ||
                grid[selectedPlayerX][selectedPlayerY][1].equals("PB"))
                && grid[i][j][1].equals("") && selectedPlayerY != j) {
            grid[selectedPlayerX][j][1] = "";
            fiftyMoveRule = 0;
        }
    }

    /**
     * Checks various conditions to determine if the game should end (checkmate, draw by stalemate,
     * draw by repetition, draw by fifty move rule, draw by insufficient material) and updates the
     * MainActivity.end string accordingly.
     *
     * @param smallerGrid The current state of the game represented as a smaller grid for evaluation purposes.
     */
    private static void handleEnd(String[][] smallerGrid)
    {
        if (King.isCheckmate(currentPlayer, smallerGrid))
        {
            MainActivity.end = "CHECKMATE";
            System.out.println("CHECKMATE");
        }
        if (King.isDrawByStalemate(currentPlayer, smallerGrid))
        {
            MainActivity.end = "DRAW BY STALEMATE";
            System.out.println("DRAW BY STALEMATE");
        }
        if (King.isDrawByRepetition())
        {
            MainActivity.end = "DRAW BY REPETITION";
            System.out.println("DRAW BY REPETITION");
        }
        if (King.isDrawByFiftyMoveRule())
        {
            MainActivity.end = "DRAW BY FIFTY MOVE RULE";
            System.out.println("DRAW BY FIFTY MOVE RULE");
        }
        if (King.isDrawByInsufficientMaterial())
        {
            MainActivity.end = "DRAW BY INSUFFICIENT MATERIAL";
            System.out.println("DRAW BY INSUFFICIENT MATERIAL");
        }
    }

    /**
     * Sets the original color (light or dark) of the grid square at position (i, j) based on its indices.
     *
     * @param i The row index of the grid square.
     * @param j The column index of the grid square.
     */
    public static void setToOriginalColor(int i, int j){
        if((i + j) % 2 == 0)
        {
            grid[i][j][0] = "L";
        }
        else {
            grid[i][j][0] = "D";
        }
    }

    /**
     * Removes all circles (en passant markers) from the grid.
     */
    public static void removeAllCircles(){
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                removeCircle(i,j);
            }
        }
    }

    /**
     * Removes all en passant markers from the grid.
     */
    public static void removeAllEnPassants(){
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (grid[i][j][1].equals("EP"))
                {
                    grid[i][j][1] = "";
                }
            }
        }
    }

    /**
     * Removes the circle marker at the specified position (i, j) in the grid.
     *
     * @param i The row index of the position.
     * @param j The column index of the position.
     */
    public static void removeCircle(int i, int j){
        grid[i][j][2] = "";
    }

    /**
     * Restarts the game by resetting all game-related variables, clearing the grid, and reinitializing it with the standard chess starting positions.
     * Also initializes grid colors based on the standard chessboard pattern.
     *
     * @return The initialized grid after restart.
     */
    public static String[][][] restartGame(){
        gridHistory = new String[70][8][8];
        moveCounter = 0;
        fiftyMoveRule = 0;
        blockAllMoves = false;
        MainActivity.end = "";
        King.whiteKingMove = false;
        King.blackKingMove = false;
        Rook.ZZrookMove = false;
        Rook.ZSrookMove = false;
        Rook.SZrookMove = false;
        Rook.SSrookMove = false;
        currentPlayer = Players.WHITE;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                for (int k = 0; k < 3; k++)
                {
                    grid[i][j][k] = "";
                }
            }
        }
        // Initialize the grid with empty strings
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if((i + j) % 2 == 0)
                {
                    grid[i][j][0] = "L";
                }
                else {
                    grid[i][j][0] = "D";
                }
            }
        }
        for (int j = 0; j < 8; j++) {
            grid[1][j][1] = "PB";
            grid[6][j][1] = "PW";
        }
        grid[0][0][1] = "RB";
        grid[0][7][1] = "RB";
        grid[0][1][1] = "KnB";
        grid[0][6][1] = "KnB";
        grid[0][2][1] = "BB";
        grid[0][5][1] = "BB";
        grid[0][3][1] = "QB";
        grid[0][4][1] = "KiB";

        grid[7][0][1] = "RW";
        grid[7][7][1] = "RW";
        grid[7][1][1] = "KnW";
        grid[7][6][1] = "KnW";
        grid[7][2][1] = "BW";
        grid[7][5][1] = "BW";
        grid[7][3][1] = "QW";
        grid[7][4][1] = "KiW";

        return grid;
    }

    /**
     * Creates a deep copy of a 3D string array.
     *
     * @param original The original 3D string array to be copied.
     * @return A new deep copy of the original 3D string array.
     */
    public static String[][][] deepCopy(String[][][] original) {
        // Create a new array with the same dimensions as the original
        String[][][] copy = new String[original.length][original[0].length][original[0][0].length];

        // Iterate through each element of the original array and copy its contents
        for (int i = 0; i < original.length; i++) {
            for (int j = 0; j < original[i].length; j++) {
                for (int k = 0; k < original[i][j].length; k++) {
                    copy[i][j][k] = original[i][j][k];
                }
            }
        }
        return copy;
    }

    /**
     * Sets the current game grid to the specified grid.
     *
     * @param setGrid The grid to set as the current game grid.
     */
    public static void setGrid(String setGrid[][][]){
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                for (int k = 0; k < 3; k++)
                {
                    grid[i][j][k] = setGrid[i][j][k];
                }
            }
        }
    }

    /**
     * Compares two 2D string arrays representing grids to check if they are identical.
     *
     * @param grid1 The first grid to compare.
     * @param grid2 The second grid to compare.
     * @return true if the grids are identical; false otherwise.
     */
    public static boolean compareGrids(String grid1[][], String grid2[][])
    {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (grid1[i][j] == null || grid2[i][j] == null || !(grid1[i][j].equals(grid2[i][j])))
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Converts a 3D chess grid into a 2D grid containing only the figures (pieces) in their current positions.
     *
     * @param grid The 3D chess grid to convert.
     * @return A 2D grid representing the figures (pieces) in their current positions.
     */
    public static String [][] convertToFigures(String grid[][][]){
        String smallerGrid[][] = new String[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                smallerGrid[i][j] = String.valueOf(grid[i][j][1]);
            }
        }
        return smallerGrid;
    }

    /**
     * Converts a 3D chess grid into a 2D grid containing only the figures (pieces) in their current positions,
     * excluding the en passant markers.
     *
     * @param grid The 3D chess grid to convert.
     * @return A 2D grid representing the figures (pieces) in their current positions, excluding en passant markers.
     */
    public static String [][] convertToFiguresWithoutEnPassant(String grid[][][]){
        String smallerGrid[][] = new String[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (grid[i][j][1].equals("EP"))
                {
                    smallerGrid[i][j] = String.valueOf("");
                }
                else
                {
                    smallerGrid[i][j] = String.valueOf(grid[i][j][1]);
                }
            }
        }
        return smallerGrid;
    }

    /**
     * Checks if there are any possible moves for a given piece at position (i, j) on the board.
     *
     * @param i           The row index of the piece.
     * @param j           The column index of the piece.
     * @param color       The color of the piece's player.
     * @param smallerGrid The 2D grid representing the current board state.
     * @return true if there are possible moves for the piece; false otherwise.
     */
    public static boolean isThereAnyPossibleMove(int i, int j, Players color, String smallerGrid[][])
    {
        if (smallerGrid[i][j].equals("PW") || smallerGrid[i][j].equals("PB"))
        {
            return Pawn.setMoves(true, i, j);
        }
        if (smallerGrid[i][j].equals("RW") || smallerGrid[i][j].equals("RB"))
        {
            return Rook.setMoves(true, i, j);
        }
        if (smallerGrid[i][j].equals("BW") || smallerGrid[i][j].equals("BB"))
        {
            return Bishop.setMoves(true, i, j);
        }
        if (smallerGrid[i][j].equals("QW") || smallerGrid[i][j].equals("QB"))
        {
            return Queen.setMoves(true, i, j);
        }
        if (smallerGrid[i][j].equals("KiW") || smallerGrid[i][j].equals("KiB"))
        {
            return King.setMoves(true, i, j);
        }
        if (smallerGrid[i][j].equals("KnW") || smallerGrid[i][j].equals("KnB"))
        {
            return Knight.setMoves(true, i, j);
        }
        return false;
    }

    /**
     * Checks if a given position (i, j) is attacked by the opponent's pieces.
     *
     * @param i           The row index of the position to check.
     * @param j           The column index of the position to check.
     * @param color       The color of the current player.
     * @param smallerGrid The 2D grid representing the current board state.
     * @return true if the position (i, j) is attacked by the opponent; false otherwise.
     */
    public static boolean isAttackedByOpposite(int i, int j, Players color, String smallerGrid[][])
    {
        // System.out.println("IS ATTACKED: " + i + " " + j);
        String oppositeColor = (color == Players.BLACK) ? "W" : "B";
        if (i < 0 || i > 7 || j < 0 || j > 7)
        {
            return false;
        }
        if (attackedByKing(i, j, smallerGrid, oppositeColor))
        {
            return true;
        }

        if (attackedByPawn(i, j, smallerGrid, oppositeColor))
        {
            return true;
        }

        if (attackedByRooks(i, j, smallerGrid, oppositeColor))
        {
            return true;
        }

        if (attackedByBishops(i, j, smallerGrid, oppositeColor))
        {
            return true;
        }

        if (attackedByKnights(i, j, smallerGrid, oppositeColor))
        {
            return true;
        }

        return false;
    }

    /**
     * Checks if the specified position (i, j) is attacked by any bishops of the opposite color
     * on the given smaller grid.
     *
     * @param i             The row index of the position.
     * @param j             The column index of the position.
     * @param smallerGrid   The 2D array representing the chess board state.
     * @param oppositeColor The color of the opposite player ('W' for white, 'B' for black).
     * @return {@code true} if the position (i, j) is attacked by any bishops of the opposite color,
     *         {@code false} otherwise.
     */
    private static boolean attackedByBishops(int i, int j, String[][] smallerGrid, String oppositeColor)
    {
        int y = j;
        for (int x = i; x < 8; x++)
        {
            if (!smallerGrid[x][y].equals(""))
            {
                if (x != i && y != j && (smallerGrid[x][y].equals("B" + oppositeColor) || smallerGrid[x][y].equals("Q" + oppositeColor)))
                {
                    return true;
                }
                if (!(smallerGrid[x][y].equals("")) && !(smallerGrid[x][y].equals("EP")) && x != i && y != j)
                {
                    break;
                }
            }
            y++;
            if (y >= 8)
            {
                break;
            }
        }
        y = j;
        for (int x = i; x < 8; x++)
        {
            if (!smallerGrid[x][y].equals(""))
            {
                if (x != i && y != j && (smallerGrid[x][y].equals("B" + oppositeColor) || smallerGrid[x][y].equals("Q" + oppositeColor)))
                {
                    return true;
                }
                if (!(smallerGrid[x][y].equals("")) && !(smallerGrid[x][y].equals("EP")) && x != i && y != j)
                {
                    break;
                }
            }
            y--;
            if (y < 0)
            {
                break;
            }
        }
        y = j;
        for (int x = i; x >= 0; x--)
        {
            if (!smallerGrid[x][y].equals(""))
            {
                if (x != i && y != j && (smallerGrid[x][y].equals("B" + oppositeColor) || smallerGrid[x][y].equals("Q" + oppositeColor)))
                {
                    return true;
                }
                if (!(smallerGrid[x][y].equals("")) && !(smallerGrid[x][y].equals("EP")) && x != i && y != j)
                {
                    break;
                }
            }
            y++;
            if (y >= 8)
            {
                break;
            }
        }
        y = j;
        for (int x = i; x >= 0; x--)
        {
            if (!smallerGrid[x][y].equals(""))
            {
                if (x != i && y != j && (smallerGrid[x][y].equals("B" + oppositeColor) || smallerGrid[x][y].equals("Q" + oppositeColor)))
                {
                    return true;
                }
                if (!(smallerGrid[x][y].equals("")) && !(smallerGrid[x][y].equals("EP")) && x != i && y != j)
                {
                    break;
                }
            }
            y--;
            if (y < 0)
            {
                break;
            }
        }
        return false;
    }

    /**
     * Checks if the specified position (i, j) is attacked by any rooks of the opposite color
     * on the given smaller grid.
     *
     * @param i             The row index of the position.
     * @param j             The column index of the position.
     * @param smallerGrid   The 2D array representing the chess board state.
     * @param oppositeColor The color of the opposite player ('W' for white, 'B' for black).
     * @return {@code true} if the position (i, j) is attacked by any rooks of the opposite color,
     *         {@code false} otherwise.
     */
    private static boolean attackedByRooks(int i, int j, String[][] smallerGrid, String oppositeColor)
    {
        for (int x = i; x < 8; x++)
        {
            if (!smallerGrid[x][j].equals(""))
            {
                if (x != i && (smallerGrid[x][j].equals("R" + oppositeColor) || smallerGrid[x][j].equals("Q" + oppositeColor)))
                {
                    return true;
                }
                if (!(smallerGrid[x][j].equals("")) && !(smallerGrid[x][j].equals("EP")) && x != i)
                {
                    break;
                }
            }
        }
        for (int x = i; x >= 0; x--)
        {
            if (!smallerGrid[x][j].equals(""))
            {
                if (x != i && (smallerGrid[x][j].equals("R" + oppositeColor) || smallerGrid[x][j].equals("Q" + oppositeColor)))
                {
                    return true;
                }
                if (!(smallerGrid[x][j].equals("")) && !(smallerGrid[x][j].equals("EP")) && x != i)
                {
                    break;
                }
            }
        }
        for (int y = j; y < 8; y++)
        {
            if (!smallerGrid[i][y].equals(""))
            {
                if (y != j && (smallerGrid[i][y].equals("R" + oppositeColor) || smallerGrid[i][y].equals("Q" + oppositeColor)))
                {
                    return true;
                }
                if (!(smallerGrid[i][y].equals("")) && !(smallerGrid[i][y].equals("EP")) && y != j)
                {
                    break;
                }
            }
        }
        for (int y = j; y >= 0; y--)
        {
            if (!smallerGrid[i][y].equals(""))
            {
                if (y != j && (smallerGrid[i][y].equals("R" + oppositeColor) || smallerGrid[i][y].equals("Q" + oppositeColor)))
                {
                    return true;
                }
                if (!(smallerGrid[i][y].equals("")) && !(smallerGrid[i][y].equals("EP")) && y != j)
                {
                    break;
                }
            }
        }
        return false;
    }


    /**
     * Checks if the specified position (i, j) is attacked by any knights of the opposite color
     * on the given smaller grid.
     *
     * @param i             The row index of the position.
     * @param j             The column index of the position.
     * @param smallerGrid   The 2D array representing the chess board state.
     * @param oppositeColor The color of the opposite player ('W' for white, 'B' for black).
     * @return {@code true} if the position (i, j) is attacked by any knights of the opposite color,
     *         {@code false} otherwise.
     */
    private static boolean attackedByKnights(int i, int j, String[][] smallerGrid, String oppositeColor)
    {
        if ((i > 1 && j > 0) && smallerGrid[i-2][j-1].equals("Kn" + oppositeColor))
        {
            return true;
        }
        if ((i > 1 && j < 7) && smallerGrid[i-2][j+1].equals("Kn" + oppositeColor))
        {
            return true;
        }
        if ((i > 0 && j > 1) && smallerGrid[i-1][j-2].equals("Kn" + oppositeColor))
        {
            return true;
        }
        if ((i > 0 && j < 6) && smallerGrid[i-1][j+2].equals("Kn" + oppositeColor))
        {
            return true;
        }
        if ((i < 6 && j > 0) && smallerGrid[i+2][j-1].equals("Kn" + oppositeColor))
        {
            return true;
        }
        if ((i < 6 && j < 7) && smallerGrid[i+2][j+1].equals("Kn" + oppositeColor))
        {
            return true;
        }
        if ((i < 7 && j > 1) && smallerGrid[i+1][j-2].equals("Kn" + oppositeColor))
        {
            return true;
        }
        if ((i < 7 && j < 6) && smallerGrid[i+1][j+2].equals("Kn" + oppositeColor))
        {
            return true;
        }
        return false;
    }

    /**
     * Checks if the specified position (i, j) is attacked by any pawns of the opposite color
     * on the given smaller grid.
     *
     * @param i             The row index of the position.
     * @param j             The column index of the position.
     * @param smallerGrid   The 2D array representing the chess board state.
     * @param oppositeColor The color of the opposite player ('W' for white, 'B' for black).
     * @return {@code true} if the position (i, j) is attacked by any pawns of the opposite color,
     *         {@code false} otherwise.
     */
    private static boolean attackedByPawn(int i, int j, String[][] smallerGrid, String oppositeColor)
    {
        if ((i < 7 && j < 7) && oppositeColor.equals("W") && smallerGrid[i+1][j+1].equals("P" + oppositeColor))
        {
            return true;
        }
        if ((i < 7 && j > 0) && oppositeColor.equals("W") && smallerGrid[i+1][j-1].equals("P" + oppositeColor))
        {
            return true;
        }
        if ((i > 0 && j < 7) && oppositeColor.equals("B") && smallerGrid[i-1][j+1].equals("P" + oppositeColor))
        {
            return true;
        }
        if ((i > 0 && j > 0) && oppositeColor.equals("B") && smallerGrid[i-1][j-1].equals("P" + oppositeColor))
        {
            return true;
        }
        return false;
    }

    /**
     * Checks if the specified position (i, j) is attacked by any king of the opposite color
     * on the given smaller grid.
     *
     * @param i             The row index of the position.
     * @param j             The column index of the position.
     * @param smallerGrid   The 2D array representing the chess board state.
     * @param oppositeColor The color of the opposite player ('W' for white, 'B' for black).
     * @return {@code true} if the position (i, j) is attacked by any king of the opposite color,
     *         {@code false} otherwise.
     */
    private static boolean attackedByKing(int i, int j, String[][] smallerGrid, String oppositeColor)
    {
        if ((i < 7) && smallerGrid[i+1][j].equals("Ki" + oppositeColor))
        {
            return true;
        }
        if ((i > 0) && smallerGrid[i-1][j].equals("Ki" + oppositeColor))
        {
            return true;
        }
        if ((j < 7) && smallerGrid[i][j+1].equals("Ki" + oppositeColor))
        {
            return true;
        }
        if ((j > 0) && smallerGrid[i][j-1].equals("Ki" + oppositeColor))
        {
            return true;
        }
        if ((i < 7 && j < 7) && smallerGrid[i+1][j+1].equals("Ki" + oppositeColor))
        {
            return true;
        }
        if ((i > 0 && j > 0) && smallerGrid[i-1][j-1].equals("Ki" + oppositeColor))
        {
            return true;
        }
        if ((i < 7 && j > 0) && smallerGrid[i+1][j-1].equals("Ki" + oppositeColor))
        {
            return true;
        }
        if ((i > 0 && j < 7) && smallerGrid[i-1][j+1].equals("Ki" + oppositeColor))
        {
            return true;
        }
        return false;
    }

    /**
     * Tests if a move from (i, j) to (nexti, nextj) is possible for a given piece on the chess board.
     *
     * @param justChecking Indicates whether to only check the move without making any changes.
     * @param i            The current row index of the piece.
     * @param j            The current column index of the piece.
     * @param nexti        The target row index of the move.
     * @param nextj        The target column index of the move.
     * @param color        The color of the piece's player ('WHITE' or 'BLACK').
     * @param smallerGrid  The 2D array representing the current board state.
     * @param figure       The type of the piece ('P', 'R', 'B', 'Q', 'Ki', 'Kn').
     * @return {@code true} if the move is possible, {@code false} otherwise.
     */
    public static boolean testForPossibleMove(boolean justChecking, int i, int j, int nexti, int nextj, Players color, String[][] smallerGrid, String figure)
    {
        boolean wasThereMove = false;
        smallerGrid[i][j] = "";
        String playerColor = (color == Players.WHITE) ? "W" : "B";
        String saveFigure = smallerGrid[nexti][nextj];
        smallerGrid[nexti][nextj] = figure + playerColor;
        if (!King.isCheck(color, smallerGrid))
        {
            if (!justChecking && !blockAllMoves)
            {
                Backend.grid[nexti][nextj][2] = "C";
            }
            wasThereMove = true;
        }
        smallerGrid[nexti][nextj] = saveFigure;
        smallerGrid[i][j] = figure + playerColor;
        return wasThereMove;
    }

}
