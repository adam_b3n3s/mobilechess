package com.example.mobilechess;

public class Pawn {

    /**
     * Calculates possible moves for the Pawn at the specified position.
     *
     * @param justChecking Indicates whether to only check the move without making any changes.
     * @param i            The current row index of the Bishop.
     * @param j            The current column index of the Bishop.
     * @return {@code true} if there are possible moves for the Bishop, {@code false} otherwise.
     */
    public static boolean setMoves(boolean justChecking, int i, int j)
    {
        boolean wasThereMove = false;
        String smallerGrid[][] = Backend.convertToFigures(Backend.grid);
        if (Backend.currentPlayer == Backend.Players.WHITE)
        {
            // En passant
            if (i == 3)
            {
                if (j != 0)
                {
                    if (Backend.grid[i - 1][j - 1][1].equals("EP"))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i - 1, j - 1, Backend.Players.WHITE, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                }
                if (j != 7)
                {
                    if (Backend.grid[i - 1][j + 1][1].equals("EP"))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i - 1, j + 1, Backend.Players.WHITE, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                }
            }
            if (i == 6)
            {
                if (Backend.grid[i - 1][j][1].equals(""))
                {
                    if (Backend.testForPossibleMove(justChecking, i, j, i - 1, j, Backend.Players.WHITE, smallerGrid, "P"))
                    {
                        wasThereMove = true;
                    }
                    if (Backend.grid[i - 2][j][1].equals(""))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i - 2, j, Backend.Players.WHITE, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                }
                if (j != 0)
                {
                    if (!Backend.grid[i - 1][j - 1][1].equals("") && Backend.grid[i - 1][j - 1][1].endsWith("B"))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i - 1, j - 1, Backend.Players.WHITE, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                }
                if (j != 7)
                {
                    if (!Backend.grid[i - 1][j + 1][1].equals("") && Backend.grid[i - 1][j + 1][1].endsWith("B"))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i - 1, j + 1, Backend.Players.WHITE, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                }
            }
            else
            {
                if (i != 0)
                {
                    if (Backend.grid[i - 1][j][1].equals(""))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i - 1, j, Backend.Players.WHITE, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                    if (j != 0)
                    {
                        if (!Backend.grid[i - 1][j - 1][1].equals("") && Backend.grid[i - 1][j - 1][1].endsWith("B"))
                        {
                            if (Backend.testForPossibleMove(justChecking, i, j, i - 1, j - 1, Backend.Players.WHITE, smallerGrid, "P"))
                            {
                                wasThereMove = true;
                            }
                        }
                    }
                    if (j != 7)
                    {
                        if (!Backend.grid[i - 1][j + 1][1].equals("") && Backend.grid[i - 1][j + 1][1].endsWith("B"))
                        {
                            if (Backend.testForPossibleMove(justChecking, i, j, i - 1, j + 1, Backend.Players.WHITE, smallerGrid, "P"))
                            {
                                wasThereMove = true;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            // En passant
            if (i == 4)
            {
                if (j != 0)
                {
                    if (Backend.grid[i + 1][j - 1][1].equals("EP"))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i + 1, j - 1, Backend.Players.BLACK, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                }
                if (j != 7)
                {
                    if (Backend.grid[i + 1][j + 1][1].equals("EP"))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i + 1, j + 1, Backend.Players.BLACK, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                }
            }
            if (i == 1)
            {
                if (Backend.grid[i + 1][j][1].equals(""))
                {
                    if (Backend.testForPossibleMove(justChecking, i, j, i + 1, j, Backend.Players.BLACK, smallerGrid, "P"))
                    {
                        wasThereMove = true;
                    }
                    if (Backend.grid[i + 2][j][1].equals(""))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i + 2, j, Backend.Players.BLACK, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                }
                if (j != 0)
                {
                    if (!Backend.grid[i + 1][j - 1][1].equals("") && Backend.grid[i + 1][j - 1][1].endsWith("W"))
                    {
                        if (Backend.testForPossibleMove(justChecking, i , j, i + 1, j - 1, Backend.Players.BLACK, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                }
                if (j != 7)
                {
                    if (!Backend.grid[i + 1][j + 1][1].equals("") && Backend.grid[i + 1][j + 1][1].endsWith("W"))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i + 1, j + 1, Backend.Players.BLACK, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                }
            }
            else
            {
                if (i != 7)
                {
                    if (Backend.grid[i + 1][j][1].equals(""))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i + 1, j, Backend.Players.BLACK, smallerGrid, "P"))
                        {
                            wasThereMove = true;
                        }
                    }
                    if (j != 0)
                    {
                        if (!Backend.grid[i + 1][j - 1][1].equals("") && Backend.grid[i + 1][j - 1][1].endsWith("W"))
                        {
                            if (Backend.testForPossibleMove(justChecking, i, j, i + 1, j - 1, Backend.Players.BLACK, smallerGrid, "P"))
                            {
                                wasThereMove = true;
                            }
                        }
                    }
                    if (j != 7)
                    {
                        if (!Backend.grid[i + 1][j + 1][1].equals("") && Backend.grid[i + 1][j + 1][1].endsWith("W"))
                        {
                            if (Backend.testForPossibleMove(justChecking, i, j, i + 1, j + 1, Backend.Players.BLACK, smallerGrid, "P"))
                            {
                                wasThereMove = true;
                            }
                        }
                    }
                }
            }
        }
        return wasThereMove;
    }
}
